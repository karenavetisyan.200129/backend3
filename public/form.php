<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Avetisyan Karen</title>
</head>

<body>
    <header>
        <div>
          <img src="https://www.kubsu.ru/sites/all/themes/portal_kubsu/logo.png" class="logo" alt="#">
          <h3>backend 3</h3>
	</div>
    </header>
    
    <div class="content">
        
        <div class="Fourth-block">
            <form method="POST" class="Form">
                <h1>Форма</h1>
                <label>
        Имя:<br>
        <input type="text" name="first-name" placeholder="Стив Джобс">
        </label><br>

        <label>
        Поле email:<br>
        <input name="field-email" placeholder="MrPropper@example.com" type="email">
        </label><br>

                <label>
        Дата рождения:<br>
        <input name="field-date" value="2001-09-11" type="date">
        </label><br>

                <label>Пол:</label><br>
                <label class="radio"><input type="radio" checked="checked" name="radio-sex" value="М">
        Мужской</label>
                <label class="radio"><input type="radio" name="radio-sex" value="Ж">
        Женский</label><br>

                <label>Кол-во конечностей:</label><br>
                <label class="radio"><input type="radio" checked="checked" name="radio-limb" value="0">
        0</label>
                <label class="radio"><input type="radio" name="radio-limb" value="1">
        1</label>
                <label class="radio"><input type="radio" name="radio-limb" value="2">
        2</label>
                <label class="radio"><input type="radio" name="radio-limb" value="3">
        3</label>
                <label class="radio"><input type="radio" name="radio-limb" value="4">
        4</label><br>

                <label>
        Ваши сверхспособности:<br>
        <select name="superpower[]" multiple="multiple">
            <option value="1">Гений</option>
            <option value="2">Миллиардер</option>
            <option value="3">Плейбой</option>
            <option value="4">Филантроп</option>
        </select>
        </label><br>

                <label>
        Биография:<br>
        <textarea name="BIO" placeholder="Расскажите о себе"></textarea>
        <br>
        </label>

        <label>
        <input name="chek" type="checkbox" checked=checked value=1> Ознакомлен с контрактом:<br>
        </label>

                <input type="submit" value="Отправить">
            </form>
        </div>
    </div>
    <footer>
        <h1>&copy; Avetisyan Karen 24/1</h1>
        <h2>student of KubSU</h2>
    </footer>
</body>



</html>
