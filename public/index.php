<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}



try{

    $errors = FALSE;
    if (empty($_POST['first-name'])) {
        print('Заполните имя.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['field-email'])) {
        print('Заполните почту.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['BIO'])) {
        print('Заполните биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['chek'])) {
        print('Вы должны быть согласны с условиями.<br/>');
        $errors = TRUE;
    }
    $ability_data = ['1', '2', '3', '4'];
    if (empty($_POST['superpower'])) {
        print('Выберите способность<br>');
        $errors = TRUE;
    }
    else {
        $abilities = $_POST['superpower'];
        foreach ($abilities as $ability) {
            if (!in_array($ability, $ability_data)) {
                print('Недопустимая способность<br>');
                $errors = TRUE;
            }
        }
    }
    if ($errors) {
        exit();
    }
    

    $name = $_POST['first-name'];
    $email = $_POST['field-email'];
    $birth = $_POST['field-date'];
    $sex = $_POST['radio-sex'];
    $limbs = $_POST['radio-limb'];
    $bio = $_POST['BIO'];
    $chek = $_POST['chek'];
    


    $conn = new PDO("mysql:host=localhost;dbname=u20553", 'u20553', '1032685', array(PDO::ATTR_PERSISTENT => true));

    
    $user = $conn->prepare("INSERT INTO form SET name = ?, email = ?, birth = ?, sex = ?, limbs = ?, bio = ?, chek = ?");
    $user -> execute([$_POST['first-name'], $_POST['field-email'], date('Y-m-d', strtotime($_POST['field-date'])), $_POST['radio-sex'], $_POST['radio-limb'], $_POST['BIO'], $_POST['chek']]);
    $id_user = $conn->lastInsertId();

    $user1 = $conn->prepare("INSERT INTO super SET id_form = ?, super_name = ?");
    foreach ($_POST['superpower'] as $value) {
        $user1->execute([$id_user, $value]);
        $result = true;
    }
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}


if ($result) {
  echo "Информация занесена в базу данных под ID №" . $id_user;
}
?>
